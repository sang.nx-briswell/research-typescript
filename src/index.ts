import {GithubApiService} from "./GithubApiService";
import {User} from "./User";
import {Repo} from "./Repo";

let svc = new GithubApiService();
svc.getUserInfo('sangnguyen', (user: User) => {
    console.log(user);
});

svc.getRepos('sangnguyen', (repos: Repo[]) => {
    console.log(repos);
})